package me.liammcguire.demobox.spring.springwebflux.controller.customer

import me.liammcguire.demobox.spring.springwebflux.model.customer.NewAddress
import me.liammcguire.demobox.spring.springwebflux.model.customer.NewCustomer
import me.liammcguire.demobox.spring.springwebflux.model.customer.StoredAddress
import me.liammcguire.demobox.spring.springwebflux.model.customer.StoredCustomer
import me.liammcguire.demobox.spring.springwebflux.model.customer.UpdateAddress
import me.liammcguire.demobox.spring.springwebflux.model.customer.UpdateCustomer

internal object CustomerObjects {
    internal fun createStoredCustomer(): StoredCustomer = StoredCustomer(
        id = "5e2234051dfd0b58632c48ed",
        forename = "John",
        surname = "Smith",
        emailAddress = "john.smith@localhost.com",
        contactNumber = "07123456789",
        address = StoredAddress(
            street = "123 Sesame Street",
            city = "Manhattan",
            postcode = "NE66 1FE"
        )
    )

    internal fun createNewCustomer(): NewCustomer = NewCustomer(
        forename = "John",
        surname = "Smith",
        emailAddress = "john.smith@localhost.com",
        contactNumber = "07123456789",
        address = NewAddress(
            street = "123 Sesame Street",
            city = "Manhattan",
            postcode = "NE66 1FE"
        )
    )

    internal fun createUpdateCustomer(): UpdateCustomer = UpdateCustomer(
        emailAddress = "john.smith123@localhost.com",
        address = UpdateAddress(
            street = "321 Sesame Street"
        )
    )
}
