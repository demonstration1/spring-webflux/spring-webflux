package me.liammcguire.demobox.spring.springwebflux.repository.customer

import me.liammcguire.demobox.spring.springwebflux.model.customer.StoredCustomer
import me.liammcguire.demobox.spring.springwebflux.repository.Repository as AbstractRepository
import org.springframework.data.mongodb.core.ReactiveMongoTemplate

class CustomerRepository(
    reactiveMongoTemplate: ReactiveMongoTemplate
) : AbstractRepository<StoredCustomer>(reactiveMongoTemplate)
