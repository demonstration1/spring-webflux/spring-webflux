package me.liammcguire.demobox.spring.springwebflux.controller.customer

import javax.validation.Valid
import me.liammcguire.demobox.spring.springwebflux.controller.AsyncCreate
import me.liammcguire.demobox.spring.springwebflux.controller.AsyncDelete
import me.liammcguire.demobox.spring.springwebflux.controller.AsyncList
import me.liammcguire.demobox.spring.springwebflux.controller.AsyncUpdate
import me.liammcguire.demobox.spring.springwebflux.controller.AsyncView
import me.liammcguire.demobox.spring.springwebflux.model.customer.NewCustomer
import me.liammcguire.demobox.spring.springwebflux.model.customer.StoredAddress
import me.liammcguire.demobox.spring.springwebflux.model.customer.StoredCustomer
import me.liammcguire.demobox.spring.springwebflux.model.customer.UpdateCustomer
import me.liammcguire.demobox.spring.springwebflux.repository.customer.CustomerRepository
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
@RequestMapping("/api/customer")
class CustomerController(
    override val repository: CustomerRepository
) : AsyncCreate<StoredCustomer>,
    AsyncList<StoredCustomer>,
    AsyncView<StoredCustomer>,
    AsyncDelete<StoredCustomer>,
    AsyncUpdate<StoredCustomer> {

    @PostMapping("/create")
    fun create(@Valid @RequestBody newCustomer: NewCustomer): Mono<ResponseEntity<StoredCustomer>> =
        super.create(StoredCustomer(
            forename = newCustomer.forename!!,
            surname = newCustomer.surname!!,
            emailAddress = newCustomer.emailAddress!!,
            contactNumber = newCustomer.contactNumber!!,
            address = StoredAddress(
                street = newCustomer.address!!.street!!,
                city = newCustomer.address.city!!,
                postcode = newCustomer.address.postcode!!
            )
        ))

    @GetMapping("/list")
    fun list(): Mono<ResponseEntity<List<StoredCustomer>>> =
        super.list(StoredCustomer::class)

    @GetMapping("/view/{id}")
    fun view(@PathVariable("id") id: String): Mono<ResponseEntity<StoredCustomer>> =
        super.view(id, StoredCustomer::class)

    @DeleteMapping("/delete/{id}")
    fun delete(@PathVariable("id") id: String): Mono<ResponseEntity<Nothing>> =
        super.delete(id, StoredCustomer::class)

    @PutMapping("/update/{id}")
    fun update(
        @PathVariable("id") id: String,
        @Valid @RequestBody updateCustomer: UpdateCustomer
    ): Mono<ResponseEntity<StoredCustomer>> = super.update(id, StoredCustomer::class) { storedCustomer ->
        storedCustomer.copy(
            forename = updateCustomer.forename.takeIf { !it.isNullOrBlank() } ?: storedCustomer.forename,
            surname = updateCustomer.surname.takeIf { !it.isNullOrBlank() } ?: storedCustomer.surname,
            emailAddress = updateCustomer.emailAddress.takeIf { !it.isNullOrBlank() } ?: storedCustomer.emailAddress,
            contactNumber = updateCustomer.contactNumber.takeIf { !it.isNullOrBlank() } ?: storedCustomer.contactNumber,
            address = storedCustomer.address.copy(
                street = updateCustomer.address?.street.takeIf { !it.isNullOrBlank() }
                    ?: storedCustomer.address.street,
                city = updateCustomer.address?.city.takeIf { !it.isNullOrBlank() }
                    ?: storedCustomer.address.city,
                postcode = updateCustomer.address?.postcode.takeIf { !it.isNullOrBlank() }
                    ?: storedCustomer.address.postcode
            ).takeIf { updateCustomer.address != null } ?: storedCustomer.address
        )
    }
}
