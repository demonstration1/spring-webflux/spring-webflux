package me.liammcguire.demobox.spring.springwebflux.controller

import kotlin.reflect.KClass
import me.liammcguire.demobox.spring.springwebflux.model.StoreableModel
import me.liammcguire.demobox.spring.springwebflux.utility.ResponseBuilder.buildResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import reactor.core.publisher.Mono

interface AsyncView<SM : StoreableModel> : RepositoryDependency<SM> {
    fun view(id: String, type: KClass<SM>): Mono<ResponseEntity<SM>> = repository.viewById(id, type)
        .map { model -> buildResponse(HttpStatus.OK, model) }
        .switchIfEmpty(Mono.just(buildResponse(HttpStatus.NOT_FOUND)))
}
