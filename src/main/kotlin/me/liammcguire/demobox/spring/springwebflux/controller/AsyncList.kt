package me.liammcguire.demobox.spring.springwebflux.controller

import kotlin.reflect.KClass
import me.liammcguire.demobox.spring.springwebflux.model.StoreableModel
import me.liammcguire.demobox.spring.springwebflux.utility.ResponseBuilder.buildResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import reactor.core.publisher.Mono

interface AsyncList<SM : StoreableModel> : RepositoryDependency<SM> {
    fun list(type: KClass<SM>): Mono<ResponseEntity<List<SM>>> = repository.list(type)
        .collectList()
        .map { models ->
            when (models.isNotEmpty()) {
                true -> buildResponse<List<SM>>(HttpStatus.OK, models)
                false -> buildResponse(HttpStatus.NOT_FOUND)
            }
        }
}
