package me.liammcguire.demobox.spring.springwebflux.model

interface StoreableModel {
    val id: String?
}
