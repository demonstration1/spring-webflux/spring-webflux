package me.liammcguire.demobox.spring.springwebflux.controller

import me.liammcguire.demobox.spring.springwebflux.model.StoreableModel
import me.liammcguire.demobox.spring.springwebflux.repository.Repository

interface RepositoryDependency<SM : StoreableModel> {
    val repository: Repository<SM>
}
