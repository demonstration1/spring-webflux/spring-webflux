package me.liammcguire.demobox.spring.springwebflux.utility

import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity

object ResponseBuilder {

    fun <T : Any> buildResponse(status: HttpStatus, body: T? = null): ResponseEntity<T> = ResponseEntity
        .status(status)
        .contentType(MediaType.APPLICATION_JSON)
        .body(body)
}
